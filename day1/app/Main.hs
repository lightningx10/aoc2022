import System.IO
import Data.List.Split
import Text.Read
import Data.Sort

type Inventory = [Integer]

getInventories :: IO [Inventory]
getInventories = do
    ls <- getLines
    let invStrs = splitOn [""] ls
    return (toInvs invStrs)
    where
        getLines :: IO [String]
        getLines = do
            buf <- hGetContents' stdin
            return (splitOn "\n" buf)
        toInvs :: [[String]] -> [Inventory]
        toInvs [] = []
        toInvs (x:xs) = toInv x : toInvs xs
        toInv :: [String] -> Inventory
        toInv [] = []
        toInv (x:xs) = case readMaybe x of
            Just i -> i : toInv xs
            Nothing -> toInv xs

getTotalCalories :: [Inventory] -> [Integer]
getTotalCalories = map (\x -> foldl (+) 0 x)

getMaxInventory :: [Inventory] -> Integer
getMaxInventory invs = maximum (getTotalCalories invs)

getThreeTop :: [Integer] -> [Integer]
getThreeTop cals = take 3 (sortBy (flip compare) cals)

main :: IO ()
main = do
    invs <- getInventories
    let m = getMaxInventory invs
    print "The maximum value is:"
    print m
    let top = getThreeTop (getTotalCalories invs)
    print "Top three:"
    print top
    print "Sum of top three:"
    print (foldl (+) 0 top)