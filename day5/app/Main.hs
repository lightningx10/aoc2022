module Main where

import Data.List.Split (splitOn)
import System.IO (hGetContents', stdin)
import Text.Read (readMaybe)
import Data.Maybe (catMaybes)

type Stack = String

getLines :: IO [String]
getLines = do
    buf <- hGetContents' stdin
    return (splitOn "\n" buf)

getISL :: [String] -> [String]
getISL ("" : _) = []
getISL (l : ls) = l : getISL ls
getISL []       = error "Fuck"

getRest :: [String] -> [String]
getRest []        = error "Fuck"
getRest ("" : ls) = ls
getRest (_ : ls)  = getRest ls

addStacks :: [Stack] -> [Stack] -> [Stack]
addStacks s1 s2 = zipWith (++) s1 s2

lineToStacks :: String -> [Stack]
lineToStacks s = map removeWS ((init . addEmpty . getStackStrings) s)
    where
        getStackStrings :: String -> [String]
        getStackStrings s = map (filter (\c -> c /= '[')) (splitOn "]" s)
        getWS :: String -> Integer
        getWS (' ' : cs) = 1 + getWS cs
        getWS _          = 0
        makeNEmpty :: Integer -> [[a]]
        makeNEmpty 0 = []
        makeNEmpty n = [] : makeNEmpty (n - 1)
        addEmpty :: [String] -> [String]
        addEmpty []       = []
        addEmpty (s : ss) = (makeNEmpty (div (getWS s) 4)) ++ s : addEmpty ss
        removeWS :: String -> Stack
        removeWS []         = []
        removeWS (' ' : cs) = removeWS cs
        removeWS (c : _)    = [c]

interpretCmdLine :: Bool -> String -> Maybe ([Stack] -> [Stack])
interpretCmdLine b s = case s of
    'm':'o':'v':'e':' ':cs -> moveFuncFromStr b cs
    otherwise              -> Nothing

moveFuncFromStr :: Bool -> String -> Maybe ([Stack] -> [Stack])
moveFuncFromStr part2 s = case splitOn " " s of
    [nS, "from", p1S, "to", p2s] -> do
        n <- readMaybe nS
        p1 <- readMaybe p1S
        p2 <- readMaybe p2s
        return (moveOp n p1 p2)
    otherwise                    -> Nothing
    where
        moveOp :: Int -> Int -> Int -> [Stack] -> [Stack]
        moveOp n p1 p2 ss = addStackTo p2 (posDrop p1 n ss) (takeFrom p1 n ss)
        takeFrom :: Int -> Int -> [Stack] -> Stack
        takeFrom pos n ss = ((if part2 then id else reverse) . head . catMaybes) (map (\(s, i) -> if i == pos then Just (take n s) else Nothing) (zip ss [1..]))
        posDrop :: Int -> Int -> [Stack] -> [Stack]
        posDrop pos n ss = map (\(s, i) -> if i == pos then drop n s else s) (zip ss [1..])
        addStackTo :: Int -> [Stack] -> Stack -> [Stack]
        addStackTo pos ss a = map (\(s, i) -> if i == pos then a ++ s else s) (zip ss [1..])

applyFList :: [(a -> a)] -> a -> [a]
applyFList [] _ = []
applyFList (f:fs) x = appl : applyFList fs appl
    where appl = f x

main :: IO ()
main = do
    ls <- getLines
    let islp1 = getISL ls
    let isl = take (length islp1 - 1) islp1
    let opStrs = getRest ls
    let (s1:ss) = map lineToStacks isl
    let initStack = foldl addStacks s1 ss
    let ops1 = catMaybes (map (interpretCmdLine False) opStrs)
    let stackStates1 = applyFList ops1 initStack
    let ops2 = catMaybes (map (interpretCmdLine True) opStrs)
    let stackStates2 = applyFList ops2 initStack
    putStrLn "Final stack state part 1:"
    print (map (\x -> head x) (last stackStates1))
    putStrLn "Final stack state part 2:"
    print (map (\x -> head x) (last stackStates2))
