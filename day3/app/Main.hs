module Main where

import System.IO
import Data.List (intersect)
import Data.List.Split
import Text.Read
import Data.Maybe (catMaybes)
import Data.Char (ord)

getLines :: IO [String]
getLines = do
    buf <- hGetContents' stdin
    return (splitOn "\n" buf)

splitLine :: String -> (String, String)
splitLine s = (take n s, drop n s)
    where
        n :: Int
        n = div (length s) 2

findCommonChars :: String -> String -> [Char]
findCommonChars = intersect

if' :: Bool -> a -> a -> a
if' True x _ = x
if' False _ y = y

getPriority :: Char -> Int
getPriority c
    | c >= 'a' && c <= 'z' = (ord c) - 96
    | c >= 'A' && c <= 'Z' = (ord c) - 38

dropEmpty :: [String] -> [String]
dropEmpty [] = []
dropEmpty (x:xs) = case x of
    "" -> dropEmpty xs
    otherwise -> x : dropEmpty xs

getGroups :: [String] -> [[String]]
getGroups [] = []
getGroups (x : []) = error "Fuck"
getGroups (x : y : []) = error "Fuck"
getGroups (x : y : z : ls) = [x, y, z] : getGroups ls

intersection :: (Eq a) => [[a]] -> [a]
intersection = foldr1 intersect

main :: IO ()
main = do
    ls <- getLines
    let ps = map (getPriority . head . (uncurry findCommonChars) . splitLine) (dropEmpty ls)
    putStrLn "Priority total part 1:"
    print (foldl (+) 0 ps)
    let bs = map (getPriority . head . intersection) (getGroups $ dropEmpty ls)
    putStrLn "Priority total part 2:"
    print (foldl (+) 0 bs)
