module Main where

import Data.List.Split (splitOn)
import System.IO (getContents, stdin)
import Text.Read (readMaybe)
import Data.Maybe (catMaybes)
import Text.Printf (printf)


getLines :: IO [String]
getLines = do
  splitOn "\n" <$> getContents

splitAtMarker :: Int -> String -> (String, String)
splitAtMarker n s = splitAtMarker' n (s, "")
    where
  splitAtMarker' :: Int -> (String, String) -> (String, String)
  splitAtMarker' n (s1, s2)
    | length s1 < n = (s1, s2)
    | otherwise     = if uniqueString (take n s1) 
                      then (drop n s1, reverse (take n s1) ++ s2)
                      else splitAtMarker' n (drop 1 s1, head s1 : s2)

uniqueString :: String -> Bool
uniqueString "" = False
uniqueString s  = uniqueString' s ""
    where
  uniqueString' :: String -> String -> Bool
  uniqueString' "" _       = True
  uniqueString' (c : cs) m = notElem c m && uniqueString' cs (c : m)

numBeforeMarker :: Int -> String -> Integer
numBeforeMarker n s = toInteger $ length charsBeforeMarker
    where
  (_, charsBeforeMarker) = splitAtMarker n s

maybeReturnParts :: [String] -> Maybe (Integer, Integer)
maybeReturnParts (s:_) = Just (numBeforeMarker 4 s, numBeforeMarker 14 s)
maybeReturnParts _     = Nothing

main :: IO ()
main = do
  ls <- getLines
  case maybeReturnParts ls of
    Just (p1, p2) -> printf "Part 1: %d\nPart2: %d\n" p1 p2
    Nothing -> printf "Input error!\n"
      
