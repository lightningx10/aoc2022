module Main where

import System.IO
import Data.List.Split
import Text.Read
import Data.Maybe (catMaybes)

data Move = Rock | Paper | Scissors
    deriving (Eq, Show)

getLines :: IO [String]
getLines = do
    buf <- hGetContents' stdin
    return (splitOn "\n" buf)

moveValue :: Move -> Integer
moveValue m
    | m == Rock = 1
    | m == Paper = 2
    | m == Scissors = 3

isWin :: Move -> Move -> Bool
isWin p1 p2
    | p1 == p2       = False
    | p1 == Rock     = case p2 of
        Paper    -> False
        Scissors -> True
    | p1 == Paper    = case p2 of
        Scissors -> False
        Rock     -> True
    | p1 == Scissors = case p2 of
        Rock  -> False
        Paper -> True
    | otherwise      = False

getScore :: Move -> Move -> Integer
getScore p1 p2
    | p1 == p2    = 3 + moveValue p1
    | isWin p1 p2 = 6 + moveValue p1
    | otherwise   = moveValue p1

moveFromString :: String -> Maybe Move
moveFromString s = case s of
    "A" -> Just Rock
    "X" -> Just Rock
    "B" -> Just Paper
    "Y" -> Just Paper
    "C" -> Just Scissors
    "Z" -> Just Scissors
    otherwise -> Nothing

losingMove :: Move -> Move
losingMove m = case m of
    Rock -> Scissors
    Paper -> Rock
    Scissors -> Paper

winningMove :: Move -> Move
winningMove m = case m of
    Rock -> Paper
    Paper -> Scissors
    Scissors -> Rock

interpretSecondString :: String -> Move -> Maybe Move
interpretSecondString s pm = case s of
    "X" -> Just (losingMove pm)
    "Y" -> Just pm
    "Z" -> Just (winningMove pm)
    otherwise -> Nothing

getMovesP1 :: [String] -> [(Move, Move)]
getMovesP1 ls = catMaybes (map lineToMoves ls)
    where
        lineToMoves :: String -> Maybe (Move, Move)
        lineToMoves l = do
            let moveStrs = splitOn " " l
            moves <- sequence (map moveFromString moveStrs)
            movePair <- tuplify moves
            return movePair

tuplify :: Eq a => [a] -> Maybe (a,a)
tuplify (x:y:ys)
    | ys == [] = Just (y, x)
    | otherwise = Nothing
tuplify x = Nothing

getMovesP2 :: [String] -> [(Move, Move)]
getMovesP2 ls = catMaybes (map lineToMoves ls)
    where
        lineToMoves :: String -> Maybe (Move, Move)
        lineToMoves l = do
            (p1Str, p2Str) <- tuplify (splitOn " " l)
            p2Move <- moveFromString p2Str
            p1Move <- interpretSecondString p1Str p2Move
            return (p1Move, p2Move)

main :: IO ()
main = do
    lines <- getLines
    let movesP1 = getMovesP1 lines
    let scoresP1 = map (\x -> uncurry getScore x) movesP1
    let totalP1 = foldl (+) 0 scoresP1
    print "Final score for part 1:"
    print totalP1
    let movesP2 = getMovesP2 lines
    let scoresP2 = map (\x -> uncurry getScore x) movesP2
    let totalP2 = foldl (+) 0 scoresP2
    print "Final score for part 2:"
    print totalP2
