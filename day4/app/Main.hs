module Main where

import Data.List (intersect)
import Data.List.Split (splitOn)
import System.IO (hGetContents', stdin)
import Text.Read (readMaybe)
import Data.Maybe (catMaybes)

type Range = [Integer]

getLines :: IO [String]
getLines = do
    buf <- hGetContents' stdin
    return (splitOn "\n" buf)

strToRange :: String -> Maybe Range
strToRange s = do
    beg <- b
    end <- e
    return ([beg..end])
    where
        b :: Maybe Integer
        b = strToInteger s1
        e :: Maybe Integer
        e = strToInteger s2
        strToInteger :: String -> Maybe Integer
        strToInteger s = readMaybe s

        ss :: [String]
        ss = splitOn "-" s
        s1 :: String
        s1 = (\(x:_) -> x) ss
        s2 :: String
        s2 = (\(_:y:_) -> y) ss

lineToRangePair :: String -> Maybe (Range, Range)
lineToRangePair l = do
    (e1, e2) <- case splitOn "," l of
        (x:y:_) -> Just (x, y)
        otherwise -> Nothing
    r1 <- strToRange e1
    r2 <- strToRange e2
    return (r1, r2)

dropEmpty :: [String] -> [String]
dropEmpty [] = []
dropEmpty (x:xs) = case x of
    "" -> dropEmpty xs
    otherwise -> x : dropEmpty xs

isFullOverlap :: (Range, Range) -> Range -> Bool
isFullOverlap (r1, r2) int = r1 == int || r2 == int

isPartialOverlap :: Range -> Bool
isPartialOverlap [] = False
isPartialOverlap _  = True

main :: IO ()
main = do
    ls <- getLines
    let rps = catMaybes (map lineToRangePair ls)
    let inters = map (uncurry intersect) rps
    let os1 = zipWith isFullOverlap rps inters
    let n1 = foldl (\x y -> x + if y then 1 else 0) 0 os1
    putStrLn "Part 1:"
    print n1
    let os2 = map isPartialOverlap inters
    let n2 = foldl (\x y -> x + if y then 1 else 0) 0 os2
    putStrLn "Part 2:"
    print n2

